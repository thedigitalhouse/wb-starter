'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

	//SVG for Everybody (ie9+, ...)
	svg4everybody();

	$('.owl-carousel').owlCarousel({
		items: 1,
    mouseDrag: false
	});

  /**
   * Call function for video play
   */
  if ( $('.video-wrap').length) {
    videoToggle();
  }

  /**
   * Popup
   */
  $('.open-popup-link').magnificPopup({
    type:'inline',
  });

  /**
   * Isotope - buzz
   */
  if ($('.grid').length) {
    $('.grid').isotope(); //solve problem with initial grid height with responsive elements
  }
  
});


//WINDOW ONLOAD
$(window).load(function() {

  if ($('.grid').length) {
    $('.grid').isotope(); //solve problem with initial grid height with responsive elements
  }

  // WINDOW RESIZE
  $(window).on('resize', function() {

  }).trigger('resize');

});

/**
 * On click hide video thumb and play video (used on step 3 page)
 */
function videoToggle() {

    $('.video-play').on("click", function (e) {
        e.preventDefault();
        // get the wrapper
        var wrapper = $(this).parents('.video-wrap');
        // get the video url and id
        var url = $(wrapper).attr('data-url');
        var id = $(wrapper).attr('data-id');

        if ( url.indexOf( 'youtu' ) !==-1 ) {
          	// set the src
          	var src = 'https://www.youtube.com/embed/' + id + '?enablejsapi=1&autoplay=1';
          	// whole player
          	var player = '<div id="video-wrap" class="embedded embedded--16by9 video-wrapper"><iframe id="video-player" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>';
        } else if ( url.indexOf( 'vimeo' ) !==-1 ) {
          	// set the src
          	var src = 'https://player.vimeo.com/video/' + id + '?autoplay=1';
          	// whole player
          	var player = '<div id="video-wrap" class="embedded embedded--16by9 video-wrapper"><iframe id="video-player" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>';
        }

        //hide video image
        wrapper.fadeOut(600);

        // play the video
        setTimeout(function() {
          $(wrapper).after(player);

        }, 600);

    });
}