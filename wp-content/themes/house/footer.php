<?php
/**
 * The footer
 *
 * Contains footer content and the closing of the
 * body and html.
 *
 * @package WordPress
 */
		/**
		 * Get subscribe section if isn't story section
		 */
		get_template_part( 'partials/site/global', 'subscribe' ); ?>

	</main><!-- #content -->

	<footer id="mainfooter" class="footer-main" role="contentinfo">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home">
			<?php echo house_image( 'logo.png', 'custom-logo', 'waste buster' ); ?>
		</a>
		
		<?php
			/**
			 * Get footer naivation
			 */
			get_template_part( 'partials/navigations/footer' );
		?>

		<div id="colophon">
			<?php
				/**
				 * Get footer copyrights
				 */
				$default = get_house_footer_copyrights();
				if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
					echo get_theme_mod( 'footer_copyright_text', $default );
				}
			?>
			<a href="http://www.thehouselondon.com">MADE AT <span class="house-char">&#8962;</span> THE HOUSE</a>
		</div><!-- #colophon -->
	</footer><!-- #mainfooter -->

<?php wp_footer(); ?>
</body>
</html>