<?php
/**
 * Template Name: Legal pages
 *
 * Page template for rendering legal pages.
 *
 * @package  WordPress
 */
get_header(); ?>

	<div class="container">

	<?php 
		/**
		 * Get the page title
		 */
		the_title( '<h1>', '</h1>' );

		/**
		 * Get flexible content fields if any,
		 * otherwise get regular content
		 *
		 * !IMPORTANT!
		 * 'all_content_fields' is loaded with all fields and,
		 * therefore, disabled. Rather create new ACF group and
		 * clone what's needed.
		 *
		 * @uses ACF Pro plugin
		 */
		if ( function_exists( 'get_field' ) && get_field( 'legal_content_fields' ) ) :

			get_template_part( 'partials/flexible-templates/sections', 'legal' );

		else :

			get_template_part( 'partials/content/post-content' );

		endif; // get_field( 'content_fields' ) ?>
		
	</div><!-- /.container -->

<?php get_footer();