<?php
/**
 * Template Name: Sections
 *
 * Page template for rendering page with flexible content.
 *
 * @package  WordPress
 */
get_header();

	/**
	 * Get flexible content fields if any,
	 * otherwise get regular content
	 *
	 * !IMPORTANT!
	 * 'all_content_fields' is loaded with all fields and,
	 * therefore, disabled. Rather create new ACF group and
	 * clone what's needed.
	 *
	 * @uses ACF Pro plugin
	 */
	if ( function_exists( 'get_field' ) && get_field( 'home_content_fields' ) ) :

		get_template_part( 'partials/flexible-templates/sections', 'home' );

	else :

		get_template_part( 'partials/content/post-content' );

	endif; // get_field( 'content_fields' )

get_footer();