<?php
/**
 * Customized dashboard
 *
 * @package WordPress
 */

/**
 * Hooks
 */
add_action( 'pre_ping', 'house_disable_self_ping' );
add_action( 'wp_before_admin_bar_render', 'house_custom_admin_bar' );
add_filter( 'admin_bar_menu', 'house_replace_howdy', 25 );
add_filter( 'admin_footer_text', 'house_admin_footer' );
add_filter( 'user_contactmethods', 'house_user_contact', 10, 1 );
// add featured image to posts and pages listings
add_filter( 'manage_posts_columns', 'house_columns_head' );
add_filter( 'manage_page_posts_columns', 'house_columns_head' );
add_action( 'manage_posts_custom_column', 'house_columns_content', 10, 2 );
add_filter( 'manage_page_posts_custom_column', 'house_columns_content', 10, 2 );
// add screen info to help tab
add_action( 'contextual_help', 'house_screen_info', 10, 3 );
// enqueue dashboard stylesheet
add_action( 'admin_enqueue_scripts', 'themename_admin_scripts_styles' );
/**
 * Load scripts and styles
 *
 * This function is attached to the admin_enqueue_scripts action hook.
 *
 */
function themename_admin_scripts_styles() {
	wp_enqueue_style( 'admin-style', get_template_directory_uri() . '/admin/css/admin-style.css', array(), '1.0' );
}

/**
 * Disable self ping
 */
function house_disable_self_ping( &$links ) {
	foreach ( $links as $l => $link ) {
		if ( 0 === strpos( $link, home_url() ) ) {
			unset( $links[$l] );
		}
	}
}
/**
 * Admin Bar
 *
 * Various admin bar customisations
 *
 */
function house_custom_admin_bar() {
	global $wp_admin_bar;

	// Removes
	// remove logo with dropdown list
	$wp_admin_bar->remove_menu('wp-logo');
}
/**
 * Replace WordPress Howdy
 */
function house_replace_howdy( $wp_admin_bar ) {
	$my_account=$wp_admin_bar->get_node( 'my-account' );
	$newtitle = str_replace( 'Howdy,', __('Welcome, you are logged in as', 'house'), $my_account->title );
	$wp_admin_bar->add_node( array(
		'id' => 'my-account',
		'title' => $newtitle,
	) );
}
/**
 * Admin Footer
 *
 * Add credits
 */
function house_admin_footer () {
	echo __('Crafted by <a href="http://www.thehouselondon.com" target="_blank">The House London</a> | Powered by WordPress', 'house');
}
/**
 * User Profile Page
 *
 * Remove deprecated contact ids and add links to profiles on currently most popular social networks.
 */
function house_user_contact( $contactmethods ) {
	$contactmethods['twitter'] = 'Twitter'; // Add Twitter
	$contactmethods['facebook'] = 'Facebook'; // Add Facebook
	$contactmethods['google'] = 'Google +'; // Add Google plus
	$contactmethods['linkedin'] = 'LinkedIn'; // Add LinkedIn
	unset($contactmethods['yim']); // Remove YIM
	unset($contactmethods['aim']); // Remove AIM
	unset($contactmethods['jabber']); // Remove Jabber

	return $contactmethods;
}
/**
 * Add a preview of featured image in posts and pages lists in dashboard
 *
 * Referenced via add_image_size() in house_setup().
 */
// get featured image
function house_get_featured_image( $post_ID ) {

	$post_thumbnail_id = get_post_thumbnail_id( $post_ID );

	if ( $post_thumbnail_id ) {
		$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'featured-preview' );
		return $post_thumbnail_img[0];
	}
}
// add new column in lists
function house_columns_head( $defaults ) {

	$defaults['featured_image'] = __('Featured Image', 'house');
	return $defaults;
}
// show featured image in new column
function house_columns_content($column_name, $post_ID) {

	if ($column_name == 'featured_image') {
		$post_featured_image = house_get_featured_image($post_ID);

		if ($post_featured_image) {
			echo '<img src="' . $post_featured_image . '" />';
		}
	}
}
/**
 * Allow HTML in Taxonomy Descriptions
 *
 * @link http://docs.appthemes.com/tutorials/allow-html-in-taxonomy-descriptions/
 *
 */
$filters = array( 'pre_term_description' );
foreach ( $filters as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
}
foreach ( array( 'term_description' ) as $filter ) {
	remove_filter( $filter, 'wp_kses_data' );
}
// Remove Filter From Additional Fields
$filters = array( 'pre_term_description', 'pre_link_description', 'pre_user_description' );
/**
 * Add screen info in contextual help (screen variable and hooks)
 *
 * @link http://wp.tutsplus.com/articles/tips-articles/quick-tip-get-the-current-screens-hooks/
 */
function house_screen_info( $contextual_help, $screen_id, $screen ) {

	// The add_help_tab function for screen was introduced in WordPress 3.3.
	if ( ! method_exists( $screen, 'add_help_tab' ) )
		return $contextual_help;

	global $hook_suffix;

	// List screen properties
	$variables = '<ul style="width:50%;float:left;"> <strong>Screen variables </strong>'
		. sprintf( '<li>Screen id : %s</li>', $screen_id )
		. sprintf( '<li>Screen base : %s</li>', $screen->base )
		. sprintf( '<li>Parent base : %s</li>', $screen->parent_base )
		. sprintf( '<li>Parent file : %s</li>', $screen->parent_file )
		. sprintf( '<li>Hook suffix : %s</li>', $hook_suffix )
		. '</ul>';

	// Append global $hook_suffix to the hook stems
	$hooks = array(
		"load-$hook_suffix",
		"admin_print_styles-$hook_suffix",
		"admin_print_scripts-$hook_suffix",
		"admin_head-$hook_suffix",
		"admin_footer-$hook_suffix"
	);

	// If add_meta_boxes or add_meta_boxes_{screen_id} is used, list these too
	if ( did_action( 'add_meta_boxes_' . $screen_id ) )
		$hooks[] = 'add_meta_boxes_' . $screen_id;

	if ( did_action( 'add_meta_boxes' ) )
		$hooks[] = 'add_meta_boxes';

	// Get List HTML for the hooks
	$hooks = '<ul style="width:50%;float:left;"> <strong>Hooks </strong> <li>' . implode( '</li><li>', $hooks ) . '</li></ul>';

	// Combine $variables list with $hooks list.
	$help_content = $variables . $hooks;

	// Add help panel
	$screen->add_help_tab( array(
		'id'      => 'house-screen-help',
		'title'   => 'Screen Information',
		'content' => $help_content,
	));

	return $contextual_help;
}
