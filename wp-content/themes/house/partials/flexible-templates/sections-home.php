<?php
/**
 * Home sections
 *
 * Template part for rendering ACF home sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'home_content_fields';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/sections/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// hero slider
	'hero_slider' => [
		'dir'      => $path,
		'template' => 'hero-slider',
	],

	// hero video
	'hero_video' => [
		'dir'      => $path,
		'template' => 'hero-video',
	],
	
	// cards
	'cards' => [
		'dir'      => $path,
		'template' => 'cards',
	],

	// leaderboard
	'leaderboard' => [
		'dir'      => $path,
		'template' => 'leaderboard',
	],

	// buzz
	'buzz' => [
		'dir'      => $path,
		'template' => 'buzz',
	],
	
	// about
	'about' => [
		'dir'      => $path,
		'template' => 'about',
	],
	
	// woobox
	'woobox' => [
		'dir'      => $path,
		'template' => 'woobox',
	],
	
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :
			
			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )