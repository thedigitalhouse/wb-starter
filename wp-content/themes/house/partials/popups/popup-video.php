<?php
/**
 * Template for popup video
 */

/**
 * Get video title
 */
acf_sub_field( 'title', true, '<h2>', '</h2>' );
/**
 * Get video paragraph
 */
acf_sub_field( 'paragraph', true, '<p>', '</p>' );

/**
 * Get video url
 */
$video_url = acf_sub_field( 'video_url', false );
/**
 * Get video id
 */
$video_id = set_video_id( $video_url );
/**
 * Cover image src - field returns url to full size
 */
$src = acf_sub_field( 'cover', false ); ?>

<div class="video-wrap" data-url="<?php echo $video_url; ?>" data-id="<?php echo $video_id; ?>" style="background-image:url('<?php echo $src; ?>');" >
	<div class="video-item">
		<span class="video-play">
			<?php echo house_svg_icon( 'play' ); ?>
		</span>
	</div><!-- / .video-item -->
</div><!-- / .video-wrap -->