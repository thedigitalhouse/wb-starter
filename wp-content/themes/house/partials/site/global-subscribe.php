<?php
/**
 * Global subscribe
 *
 * Template part for rendering subscribe form.
 *
 * @package WordPress
 */
?>
<section id="subscribe" class="subscribe">
	<div class="container">
		<div class="flex-row flex-row--gutter-none">
			<div class="col-6">
				<?php
					/**
					 * Get site logo and/or
					 * site title and tagline
					 */
					if ( function_exists( 'the_custom_logo' ) ) {
						the_custom_logo();
					}
				?>
			</div><!-- /.col-6 -->
			<div class="col-6">
				<div class="subscribe__item">
					<?php
						/**
						 * Get section title
						 */
						if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
							echo '<h2>' . get_theme_mod( 'subscribe_title' ) . '</h2>';
						}
						/**
						 * Get subscribe form
						 */
						get_template_part( 'partials/forms/mailchimp' );
					?>

					<ul class="social-networks">
						<?php
							/**
							 * Get social profiles links
							 */
							get_template_part( 'partials/site/subscribe', 'social' );
						?>
					</ul>

					<div class="contact-info">
						<?php
							/**
							 * Get contact info
							 */
							if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
								echo '<p>
										<span class="contact-phone-prefix">' . get_theme_mod( 'phone_number_prefix' ) . '</span>
										<span class="contact-phone">' . get_theme_mod( 'phone_number' ) . '</span>
									</p>

									<p>
										<span class="contact-email-prefix">' . get_theme_mod( 'email_prefix' ) . '</span>
										<span class="contact-email"><a href="mailto:' . get_theme_mod( 'contact_email_address' ) . '">' . get_theme_mod( 'contact_email_address' ) . '</a></span>
									</p>';
							}
						?>
					</div><!-- /.contact-info -->
				</div><!-- /.subscribe-item -->
			</div><!-- /.col-6 -->
		</div><!-- /.flex-row -->
	</div><!-- /.container -->
</section><!-- /.subscribe -->
