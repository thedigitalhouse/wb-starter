<?php
/**
 * Global social
 *
 * Template part for rendering social profile links.
 *
 * @package WordPress
 */

/**
 * Get all populated social links from customizer
 *
 * There are other helper functions available, e.g. get specific
 * profile link or just url etc. @see inc/branding/social.php
 */
if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
	/**
	 * We can't use just one function for all social links
	 * all_social_profiles() because of the links order and
	 * different naming for icons, so we have to call them one by one
	 *
	 * Translators:
	 *     name of network (as field id in customizer),
	 *     class for the link,
	 *     icon (text will appear if none set),
	 *     custom markup/text for before and after the link
	 */
	social_profile_link( 'twitter', 'social-links', house_svg_icon( 'twitter' ), '<li>', '</li>' );
	social_profile_link( 'facebook', 'social-links', house_svg_icon( 'fb' ), '<li>', '</li>' );
	social_profile_link( 'google', 'social-links', house_svg_icon( 'google' ), '<li>', '</li>' );
	social_profile_link( 'instagram', 'social-links', house_svg_icon( 'instagram' ), '<li>', '</li>' );
}