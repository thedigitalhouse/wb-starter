<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<div class="site-nav-right">
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<?php wp_nav_menu( array( 'menu_class' => 'menu', 'container' => 'ul', 'theme_location' => 'header' ) ); ?>
	</nav><!-- #site-navigation -->
</div><!-- /.site-nav-right -->