<?php
/**
 * Heading
 *
 * Template part for rendering ACF flexible sections - heading
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'heading', true, '<h1>', '</h1>' );

