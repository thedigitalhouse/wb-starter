<?php
/**
 * About
 *
 * Template part for rendering ACF flexible sections - about
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */ ?>
<section class="about">
	<div class="container">

		<?php
		if ( have_rows( 'about_content_fields' ) ) : while ( have_rows( 'about_content_fields' ) ) : the_row();
			/**
			 * Get about heading
			 */
			acf_sub_field( 'heading', true, '<h2>', '</h2>' );
			/**
			 * Get about sub heading
			 */
			acf_sub_field( 'sub_heading', true, '<h3>', '</h3>' );
			/**
			 * Get about paragraph
			 */
			acf_sub_field( 'paragraph', true, '<p>', '</p>' );
			/**
			 * Get about paragraph
			 */
			acf_sub_field( 'call_to_action', true, '<p><strong>', '</strong></p>' );
			
			if ( acf_sub_field( 'button_url', false ) && acf_sub_field( 'button_label', false ) ) :
				
				/**
				 * Get button label
				 */
				$label = acf_sub_field( 'button_label', false );
				/**
				 * Get button url
				 */
				$url = acf_sub_field( 'button_url', false );

				echo '<a href="' . $url . '">' . $label . '</a>';
			
			endif;

		endwhile; endif; ?>

	</div><!-- /.container -->
</section>