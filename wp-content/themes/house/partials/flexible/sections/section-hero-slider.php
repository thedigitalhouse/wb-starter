<?php
/**
 * Hero Slider
 *
 * Template part for rendering ACF flexible sections - hero-slider
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */ ?>
<section class="hero">

	<?php if ( have_rows( 'slider' ) ) : ?>

		<div class="owl-carousel owl-theme">

		<?php while ( have_rows( 'slider' ) ) : the_row(); ?>

			<div class="item">

				<?php
				/**
				 * Translators: field id, echo (false returns value), before, after
				 */
				$image = acf_sub_field( 'image', false );

				if ( $image ) :
					/**
					 * Prepare sizes
					 * @var string
					 */
					$thumbnail = $image['sizes']['thumbnail'];
					$medium    = $image['sizes']['medium'];
					$large     = $image['sizes']['large'];
					$full      = $image['url'];

					echo '<img src="' . $full . '" />';
					
					/**
					 * Get heading H1
					 */
					acf_sub_field( 'heading_h1', true, '<h1>', '</h1>' );
					/**
					 * Get paragraph
					 */
					acf_sub_field( 'paragraph', true, '<p>', '</p>' );
				
					if ( acf_sub_field( 'button_label', false ) ) :
						/**
						 * Get the button label
						 */
						$label = acf_sub_field( 'button_label', false );
						/**
						 * Get button url
						 */
						$url = acf_sub_field( 'button_url', false );

						echo '<a href="' . $url . '">' . $label . '</a>';

					endif;

				endif; // $image ?>

			</div><!-- /.item -->

		<?php endwhile; ?>

		</div><!-- /.owl-carousel -->

	<?php endif; ?>

</section>