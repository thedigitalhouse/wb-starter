<?php
/**
 * Hero Video
 *
 * Template part for rendering ACF flexible sections - hero-video
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */ 
if ( acf_sub_field( 'video_url', false ) ) : ?>

	<section class="hero">

		<?php
		/**
		 * Get video url
		 */
		$video_url = acf_sub_field( 'video_url', false );
		/**
		 * Get video id
		 */
		$video_id = set_video_id( $video_url );
		/**
		 * Cover image src - field returns url to full size
		 */
		$src = acf_sub_field( 'cover', false );
		/**
		 * Get button label
		 */
		if ( acf_sub_field( 'button_label', false ) ) :
			$label = acf_sub_field( 'button_label', false );
		else :
			$label = __( 'Video play', 'house' );
		endif; ?>

		<div class="video-wrap" data-url="<?php echo $video_url; ?>" data-id="<?php echo $video_id; ?>" style="background-image:url('<?php echo $src; ?>');" >
			<div class="video-item">
				<span class="video-play"><button class="btn btn-play"></button><?php echo $label; ?></span>
				<?php
					/**
					 * Video title
					 * Translators: field id, echo (false returns value), before, after, post id
					 */
					acf_sub_field( 'video_title', true, '<h3 class="video-title">', '</h3>' );
				?>
			</div><!-- / .video-item -->
		</div><!-- / .video-wrap -->

	</section>

<?php endif;