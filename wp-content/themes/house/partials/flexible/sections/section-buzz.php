<?php
/**
 * Buzz
 *
 * Template part for rendering ACF flexible sections - buzz
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */ 
/**
 * Get the query
 * @var obj
 */
$query = get_ajax_query();

if ( $query->have_posts() ) : ?>

<section class="buzz-section">
	<div class="container">
		<?php
			/**
			 * Get section title
			 */
			acf_sub_field( 'heading', true, '<h2>', '</h2>' );
			/**
			 * Get section paragraph
			 */
			acf_sub_field( 'paragraph', true, '<p>', '</p>' );
			/**
			 * Get hashtags
			 */
			acf_sub_field( 'hashtags', true, '<h4 class="buzz-subhead">', '</h4>' );
		?>

		<div id="ajax-posts" class="grid">

			<?php
				/**
				 * Get posts
				 */
				while ( $query->have_posts() ) : $query->the_post();
					get_template_part( 'content', 'buzz' );
				endwhile; // $query->have_posts()
			?>

		</div><!-- /.grid -->

		<?php
			/**
			 * Get 'posts_per_page' setting
			 * @var int
			 */
			$posts_per_page = $query->query_vars['posts_per_page'];
			/**
			 * Get total number of posts found by query
			 * @var int
			 */
			$found_posts = $query->found_posts;
			/**
			 * Show load more button only if
			 * number of found posts if larger than 'posts_per_page'.
			 * We don't want button that does nothin'.
			 */
			if ( $found_posts > $posts_per_page ) :
				/**
				 * Set the button label
				 */
				if ( get_sub_field( 'button_label' ) ) {
					$label = get_sub_field( 'button_label' );
				} else {
					$label = __( 'Load more', 'house' );
				} ?>

				<div class="text-center">
					<button id="more_posts" class="btn btn--primary btn--border"><?php echo $label; ?></button>
				</div><!-- text-center -->
			<?php endif; //$found_posts > $posts_per_page
		?>

	</div><!-- /.container -->
</section><!-- /.buzz-section -->

<?php endif; // $query->have_posts()
/**
 * Always reset custom queries
 */
wp_reset_postdata();