<?php
/**
 * Leaderboard
 *
 * Template part for rendering ACF flexible sections - leaderboard
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */ ?>
<section class="leaderboard">
	<div class="container">
		<?php
			acf_sub_field( 'heading', true, '<h2>', '</h2>' );
			acf_sub_field( 'paragraph', true, '<p>', '</p>' );

			/**
			 * Get schools from CPT
			 * @var array
			 */
			$args = array( 'post_type' => 'school', 'orderby' => 'title', 'order' => 'ASC' );
			$query = new WP_query( $args );
			/**
			 * List all schools from database
			 */
			if ( $query->have_posts() ) : 

				echo '<ul>';

					while ( $query->have_posts() ) : $query->the_post();

						echo '<li>' . get_the_title() . '</li>';

					endwhile; 

				echo '</ul>';

			endif;
			wp_reset_postdata();
		?>
	</div><!-- /.container -->
</section>