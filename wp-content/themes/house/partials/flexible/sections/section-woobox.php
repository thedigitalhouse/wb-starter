<?php
/**
 * Woobox
 *
 * Template part for rendering ACF flexible sections - woobox
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
global $globalSite; ?>

<section class="woobox">
	<div class="container">
		
		<?php
		/**
		 * Print title
		 */
		acf_sub_field( 'heading', true, '<h2>', '</h2>' );
		
		/**
		 * Get woobox promo code
		 */
		$promo_code = acf_sub_field( 'promo_id', false );
		?>
		
		<iframe src='https://woobox.com/<?php echo $promo_code; ?>/vote' 
			frameborder='0'
			width='100%'
			height="1000"
			scrolling='no'>
		</iframe>

	</div><!-- /.container -->
</section>