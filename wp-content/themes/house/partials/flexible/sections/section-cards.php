<?php
/**
 * Cards
 *
 * Template part for rendering ACF flexible sections - cards
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
global $globalSite; ?>

<section class="cards">
	<div class="container">
		
		<?php
		/**
		 * Get content's heading
		 */
		acf_sub_field( 'heading', true, '<h2>', '</h2>' );
		/**
		 * Get content's paragraph
		 */
		acf_sub_field( 'paragraph', true, '<p>', '</p>' );

		$i = 0;
		if ( have_rows( 'cards' ) ) : ?>

			<div class="flex-row">

			<?php while ( have_rows( 'cards' ) ) : the_row(); ?>

				<div class="col-4">
					<div class="card">

						<?php 
							$i++;
							/**
							 * Translators: field id, echo (false returns value), before, after
							 */
							$image = acf_sub_field( 'image', false );

							if ( $image ) :
								/**
								 * Prepare sizes
								 * @var string
								 */
								$thumbnail = $image['sizes']['thumbnail'];
								$medium    = $image['sizes']['medium'];
								$large     = $image['sizes']['large'];
								$full      = $image['url']; ?>
								
								<div class="img-wrap">
									<img src="<?php echo $medium; ?>" alt="" />
								</div><!-- /.img-wrap -->

							<?php endif; // $image
					
							/**
							 * Get card's heading
							 */
							acf_sub_field( 'heading', true, '<h4>', '</h4>' );
							/**
							 * Get card's paragraph
							 */
							acf_sub_field( 'paragraph', true, '<p>', '</p>' );
							/**
							 * Get card's button label
							 */
							if ( acf_sub_field( 'button_label', false ) ) :

								echo '<a href="#card-popup-' . $i . '" class="open-popup-link">' . acf_sub_field( 'button_label', false ) . '</a>';

								/**
								 * Get video popup
								 */
								if ( acf_sub_field( 'button_action', false ) == 'video' ) : 

									if ( have_rows( 'video' ) ) : while ( have_rows( 'video' ) ) : the_row(); ?>

										<div id="card-popup-<?php echo $i; ?>" class="white-popup mfp-hide">
										  	
											<?php get_template_part( 'partials/popups/popup', 'video' ); ?>

										</div>

									<?php endwhile; endif;

								endif;
								/**
								 * Get download form popup
								 */
								if ( acf_sub_field( 'button_action', false ) == 'download-form' ) : ?>

									<div id="card-popup-<?php echo $i; ?>" class="white-popup mfp-hide">
									  	
										<?php get_template_part( 'partials/forms/download' ); ?>

									</div>

								<?php endif;
								/**
								 * Get report form popup
								 */
								if ( acf_sub_field( 'button_action', false ) == 'report-form' ) : ?>

									<div id="card-popup-<?php echo $i; ?>" class="white-popup mfp-hide">
									  	
										<?php get_template_part( 'partials/forms/report' ); ?>

									</div>

								<?php endif;

							endif; ?>

						</div><!-- /.card -->
					</div><!-- /.col-4 -->

				<?php endwhile; ?>

			</div><!-- /.flex-row -->

		<?php endif; 

		if ( ! empty( $_POST ) ) : ?>

			<div id="msg" class="subscribe-message-wrap">
				<div class="subscribe-message subscribe-message--<?php echo $globalSite['class']; ?>">
					<span class="subscribe-message__text"><?php echo $globalSite['message']; ?></span>
					<span class="subscribe-message__close"><a href="javascript:;"><?php echo house_svg_icon( 'close' ); ?></a></span>
				</div><!-- end of .subscribe-message -->
			</div><!-- end of .subscribe-message-wrap -->

		<?php endif; ?>

	</div><!-- /.container -->
</section>