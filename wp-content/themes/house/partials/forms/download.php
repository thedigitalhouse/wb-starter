<?php
/**
 * Download
 *
 * Template part for rendering Download form
 *
 * @package WordPress
 */

if ( !empty( $_POST ) && isset( $_POST['download_full_name'] ) ) :
	// NB - we need to escape these values before echoing them in the template
	// DO NOT echo raw post data in the template, it can be used for XSS attack
	$download_full_name = sanitize_text_field( $_POST['download_full_name'] );
	$download_parents_email = sanitize_text_field( $_POST['download_parents_email'] );
	$download_school_name = sanitize_text_field( $_POST['download_school_name'] );

	// All submitted schools need to be checked by moderator
    house_insert_school( $download_school_name );
else :
    $download_full_name = NULL;
    $download_parents_email = NULL;
    $download_school_name = NULL;
endif;

?>
<form action="" method="post">
	<input type="text" name="download_full_name" value="<?php echo $download_full_name; ?>" placeholder="<?php esc_attr_e( 'Full Name', 'house' ); ?>" required /><br />
	<input type="email" name="download_parents_email" value="<?php echo $download_parents_email; ?>" placeholder="<?php esc_attr_e( 'Parent\'s Email Address', 'house' ); ?>" required /><br />
	<input id="schools" name="download_school_name" value="<?php echo $download_school_name; ?>" placeholder="<?php esc_attr_e( 'School Name', 'house' ); ?>" required /><br />
	
	<script>
	$( function() {
	  var availableTags = [
		  
		<?php
			/**
			 * Get schools from CPT
			 * @var array
			 */
			$args = array( 'post_type' => 'school', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1 );
			$query = new WP_query( $args );
			// Array for schools
			$schools = [];
			/**
			 * List all schools from database
			 */
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

			  $schools[] =  '"' . get_the_title() . '"';

			endwhile; endif;
			wp_reset_postdata();
			
			echo implode(',', $schools);
		?>
		
	  ];
	  $( "#schools" ).autocomplete({
		source: availableTags
	  });
	} );
	</script>	
		
	<input type="submit" value='<?php esc_attr_e( 'Download Mission', 'house' ); ?>'>
</form>
