<?php
/**
 * Report
 *
 * Template part for rendering Report form
 *
 * @package WordPress
 */
/**
 * These are needed as dependencies in order to upload file to Media
 *
 * Must be called before the field for upload
 *
 * @link https://codex.wordpress.org/Function_Reference/media_handle_upload
 */
require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
require_once( ABSPATH . "wp-admin" . '/includes/media.php' );

global $globalSite;
?>
<form action="#msg" method="post" enctype="multipart/form-data">
	<input type="text" name="report_full_name" placeholder="<?php esc_attr_e( 'Full Name', 'house' ); ?>" required /><br />
	<input type="email" name="report_parents_email" placeholder="<?php esc_attr_e( 'Parent\'s Email Address', 'house' ); ?>" required /><br />
	<input list="schools" name="report_school_name" placeholder="<?php esc_attr_e( 'School Name', 'house' ); ?>" required /><br />
	<datalist id="schools">
		<?php
			/**
			 * Get schools from CPT
			 * @var array
			 */
			$args = array( 'post_type' => 'school', 'orderby' => 'title', 'order' => 'ASC' );
			$query = new WP_query( $args );
			/**
			 * List all schools from database
			 */
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

			  echo '<option value="' . get_the_title() . '">';

			endwhile; endif;
			wp_reset_postdata();
		?>
	</datalist>
	<input type="file" name="report_add_photo" /><br />
	<input type="checkbox" name="report_terms" required /> <?php _e( 'I agree to the Terms & Conditions', 'house' ); ?><br />
	<input type="checkbox" name="report_mailing_list" /> <?php _e( 'Subscribe to the mailing list', 'house' ); ?><br />
	
	<input type="submit" value='<?php esc_attr_e( 'Download Mission', 'house' ); ?>'>
</form>

<?php
	/**
	 * Declare empty globals
	 * to avoid notices
	 */
	$globalSite['report_full_name'] = '';
	$globalSite['report_parents_email'] = '';
	$globalSite['report_school_name'] = '';
	$globalSite['report_terms'] = false;
	$globalSite['report_mailing_list'] = false;
	$globalSite['message'] = '';
	$globalSite['class'] = '';

	if ( isset( $_POST ) && ! empty( $_POST ) ) :

		/**
		 * Setup text input fields
		 * not sure if datalist can go inside, we should check
		 * @var array
		 */
		$text_fields = [
			'report_full_name',
			'report_parents_email',
			'report_school_name'
		];

		foreach ( $text_fields as $text ) :

			if ( isset( $_POST[$text] ) && ! empty( $_POST[$text] ) ) :
				$globalSite[$text] = $_POST[$text];
			endif;

		endforeach; // $text_fields as $text
		/**
		 * Set up bool fields
		 * @var array
		 */
		$bool_fields = [
			'report_terms',
			'report_mailing_list'
		];

		/**
		 * Get name
		 * @var string
		 */
		$name = house_check_value( $globalSite['report_full_name'], false );

		/**
		 * Get parent's email
		 */
		$email = house_check_value( $globalSite['report_parents_email'], false );

		/**
		 * Get school name
		 */
		$school = house_check_value( $globalSite['report_school_name'], false );	

		/**
		 * Set custom fields for new post
		 * @var array
		 */
		$meta = array(
			'report_full_name'   => $name,
			'report_parents_email' => $email,
			'report_school_name' => $school
		);

		/**
		 * Create new draft report post
		 * Translators: name (used for post title), array of custom fields
		 */
		report_insert_post( $email, $meta );
		/**
		 * Pack values into our global
		 */
		$globalSite['report_array'] = $meta;

		foreach ( $bool_fields as $bool ) :

			if ( isset( $_POST[$bool] ) && ! empty( $_POST[$bool] ) ) :
				$globalSite[$bool] = true;
			endif;

		endforeach; // $bool_fields as $bool

		if ( isset( $_POST['report_mailing_list'] ) && $_POST['report_mailing_list'] == 'on' ) :

			/**
			 * API Key
			 * Account -> Extras -> API Keys
			 *
			 * @var MailChimp
			 */
			$apikey = get_theme_mod( 'mailchimp_api_key' );
			/**
			 * List ID
			 * @var string
			 */
			$list_id = get_theme_mod( 'mailchimp_list_id' );
			/**
			 * Get the MailChimp response
			 * @var array
			 */
			$response = get_mailchimp_response( $list_id, $_POST['report_parents_email'], $apikey );

			/**
			 * If status is not 'subscribed' we have error
			 */
			if ( $response['status'] !== 'subscribed' ) {
				// user feedback message
				$message = sprintf( __( 'Error: %s.', 'house' ), $response['title'] );
				// message class
				$class = 'error1';

			} else {

				// user feedback message
				if ( get_theme_mod( 'mailchimp_thank_you_message' ) ) {
					$message = get_theme_mod( 'mailchimp_thank_you_message' );
				} else {
					$message = __( 'Thank you for subscribing.', 'house' );
				}

				// message class
				$class = 'success';
			}
		
			$globalSite['message'] = $message;
			$globalSite['class'] = $class;

		else :

			/**
			 * Check if parent's email exists in database
			 * return warning message
			 */
			$args = array(
				'post_type' => 'report'
			);
			/**
			 * Custom query
			 */
			$query = new WP_Query( $args );

			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();


				if ( custom_meta( 'report_parents_email', false ) == $_POST['report_parents_email'] ) :

					$globalSite['message'] = __( 'This email exists in database.', 'house' );

				else :

					$globalSite['message'] = __( 'Your data has been sent.', 'house' );

				endif;
				
			endwhile; endif;
			wp_reset_postdata();
		
		endif;

	endif; // isset( $_POST )