<?php
/**
 * Helper functions for taxonomies and posts (cpt) in general
 *
 * @package WordPress
 */

/**
 * Get posts from custom post type type
 *
 * Custom query for 'custom post type.
 *
 * @return Error|obj Returns WP_Query object
 */
function get_cpt_posts( $post_type, $posts_per_page ) {
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$args = array(
		'post_type'      => $post_type,
		'posts_per_page' => $posts_per_page,
		'paged'          => $paged
	);

	$query = new WP_Query( $args );

	return $query;
}

/**
 * Single Term Slug
 *
 * Get single term slug, as seen with single_term_title().
 *
 * @param  string  $prefix     Before term slug
 * @param  boolean $display    Echo or return
 * @return bool|string         Returns term slug if exists
 */
function single_term_slug( $prefix = '', $display = true ) {
	$term = get_queried_object();

	if ( ! $term ) {
		return;
	}

	if ( is_category() ) {
		$term_slug = apply_filters( 'single_cat_slug', $term->slug );
	} elseif ( is_tag() ) {
		$term_slug = apply_filters( 'single_tag_slug', $term->slug );
	} elseif ( is_tax() ) {
		$term_slug = apply_filters( 'single_term_slug', $term->slug );
	} else {
		return;
	}

	if ( empty( $term_slug ) ) {
		return;
	}

	if ( $display ) {
		echo $prefix . $term_slug;
	} else {
		return $term_slug;
	}
}