<?php
/**
 * Schools custom post type
 *
 * @package WordPress
 */
/**
 * Set supports array
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#supports
 * @var array
 */
$supports = array(
	'title',
	'thumbnail',
	'custom-fields'
);
/**
 * Register custom post type with custom options
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 5,
	'supports'      => $supports,
	'has_archive'   => true
);
/**
 * Create the custm post type
 *
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'school', $options );
/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$cpt->menu_icon( 'dashicons-building' );
/**
 * Dashboard posts listing columns
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#columns
 */
$cpt->columns( array(
	'cb'            => '<input type="checkbox" />',
	'title'         => __( 'Title', 'house' ),
	'date'          => __( 'Date', 'house' ),
	'featured'      => __( 'Featured Image', 'house' ),
));