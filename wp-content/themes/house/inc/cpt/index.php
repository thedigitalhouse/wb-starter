<?php
/**
 * Here include all custom post type related functions
 */

 // cpt and taxonomy helper functions
 include( get_template_directory() . '/inc/cpt/taxonomies-cpt-functions.php' );
 // school custom post type
 include( get_template_directory() . '/inc/cpt/schools-cpt.php' );
  // report custom post type
 include( get_template_directory() . '/inc/cpt/report-cpt.php' );
