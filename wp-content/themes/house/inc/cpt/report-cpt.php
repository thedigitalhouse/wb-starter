<?php
/**
 * Report custom post type
 *
 * @package WordPress
 */
/**
 * Set supports array
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#supports
 * @var array
 */
$supports = array(
	'title',
	'thumbnail',
	'custom-fields'
);
/**
 * Register custom post type with custom options
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 5,
	'supports'      => $supports,
	'has_archive'   => true
);
/**
 * Create the custom post type
 *
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'report', $options );
/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$cpt->menu_icon( 'dashicons-chart-area' );
/**
 * Dashboard posts listing columns
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#columns
 */
$cpt->columns( array(
	'cb'            => '<input type="checkbox" />',
	'title'         => __( 'Title', 'house' ),
	'date'          => __( 'Date', 'house' ),
	'featured'      => __( 'Featured Image', 'house' ),
	'report_school_name'   => __( 'School name', 'house' ),
));
/**
 * Populate custom columns
 *
 * Default 'icon' column is using 'thumbnail' size which is too big, so we are creating
 * custom column with our predefined smaller image size (100px x 75px).
 *
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#populating-columns
 */
$cpt->populate_column( 'featured', function( $column, $post ) {
	if ( has_post_thumbnail( $post ) ) {
		$src = get_featured_image_src( $post, 'featured-preview' );
		echo '<img src="' . $src . '" />';
	}
});
/**
 * Get school name
 *
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#populating-columns
 */
$cpt->populate_column( 'report_school_name', function( $column, $post ) {
	custom_meta( 'report_school_name', true, '<h4>', '</h4>' );
});
/**
 * Insert report post
 *
 * Create new report post on report form submission.
 *
 * @param  string  $email   Email address, used for post title
 * @param  array  $meta     Array of post meta fields
 * @return int|Error        Returns post id or Error
 */
function report_insert_post( $email = '', $meta = array() ) {
	/**
	 * Check is we have report posted by specified email
	 * and create new report if not
	 */
	if ( ! get_page_by_title( $email, 'OBJECT', 'report' ) ) :

		$report = array(
			'post_type'     => 'report',
			'post_status'   => 'draft', // this is default but nevertheless
			'post_title'    => $email,
			'meta_input'    => $meta,
		);
		/**
		 * Create report post
		 *
		 * Post author is not defined, will be admin.
		 *
		 * @link https://developer.wordpress.org/reference/functions/wp_insert_post/
		 * @return int|WP_Error The post ID on success. The value 0 or WP_Error on failure.
		 */
		$report_id = wp_insert_post( $report );

		if ( isset( $_FILES['report_add_photo'] ) && ! empty( $_FILES['report_add_photo'] ) ) :
			/**
			 * Upload image to Media dir and attach it to newly created report
			 *
			 * Get $_FILES array of the uploaded file and use data to:
			 * 1. upload file to /wp-content/uploads/ dir, available in Media library
			 * 2. attach file to the report post
			 *
			 * 'report_add_photo' is the 'name' attribute for input type 'file'
			 * from which we collect the media file.
			 *
			 * @link https://codex.wordpress.org/Function_Reference/media_handle_upload
			 * @return int|WP_Error The ID of the attachment, or a WP_Error if the upload failed.
			 */
			$attachment_id = media_handle_upload( 'report_add_photo', $report_id );
			/**
			 * Set attachment as featured image for newly created report post
			 *
			 * Get the existing image from Media library and set it
			 * as featured image for the report post.
			 *
			 * @link https://codex.wordpress.org/Function_Reference/set_post_thumbnail
			 * @return Post meta ID on success, false on failure.
			 */
			set_post_thumbnail( $report_id, $attachment_id );
		endif; // isset( $_FILES['report_add_photo'] )

	endif; // get_page_by_title( $email, 'OBJECT', 'report' )
}