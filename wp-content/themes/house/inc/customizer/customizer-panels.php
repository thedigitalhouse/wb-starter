<?php
/**
 * Customizer: Add Panels
 *
 * @package WordPress
 */
/**
 * Register customizer panels
 *
 * Adds custom panels to customizer.
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_register_customizer_panels( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Add Theme Options panel
	 *
	 * @uses $wp_customize->add_panel() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_panel/
	 * @link $wp_customize->add_panel() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_panel
	 */
	$wp_customize->add_panel( 'house_options_panel', array(
		'title'       => __( 'Theme Options', 'house' ),
		'description' => __( 'Configure your theme settings', 'house' ),
	) );
}

add_action( 'customize_register', 'house_register_customizer_panels', 11 );

/**
 * Customize preview init
 *
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * This function is attached to 'customize_preview_init' action hook.
 */
function house_customize_preview_js() {
	/**
	 * Load custom js for selective refresh
	 */
	wp_enqueue_script( 'house-customizer', get_template_directory_uri() . '/inc/customizer/js/customizer.js', array( 'jquery','customize-preview' ), '1.0.0', true );
	/**
	 * Load styles for small tweaks of customizer's UI
	 */
	wp_enqueue_style( 'house-customizer', get_template_directory_uri() . '/inc/customizer/css/customizer.css' );
}
add_action( 'customize_preview_init', 'house_customize_preview_js' );
