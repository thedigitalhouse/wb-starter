<?php
/**
 * Customizer: Settings and controls for 'MailChimp options' section
 *
 * @package WordPress
 */
/**
 * Register settings and controls for 'MailChimp options' section
 *
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_register_customizer_section_mailchimp( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Subscribe section title
	 *
	 * - Setting: Subscribe section title
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Subscribe section title.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'subscribe_title', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Subscribe section title control
	 *
	 * - Control: Basic: Text
	 * - Setting: Subscribe section title
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Subscribe section title setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'subscribe_title', array(
		'label'       => __( 'Subscribe section title', 'house' ),
		'description' => __( 'Title for subscribe section', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'subscribe_title',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp API Key setting
	 *
	 * - Setting: Mailchimp API Key
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp API Key.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_api_key', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp API Key control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp API Key
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp API Key setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_api_key', array(
		'label'       => __( 'Mailchimp API Key', 'house' ),
		'description' => __( 'Paste your Mailchimp API Key here.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_api_key',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp Report List ID setting
	 *
	 * - Setting: Mailchimp Report List ID
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp List ID.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_list_id', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Report List ID control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Report List ID
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp List ID setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_list_id', array(
		'label'       => __( 'Mailchimp Report List ID', 'house' ),
		'description' => __( 'Paste your Mailchimp List ID here.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_list_id',
		'type'        => 'text',
	) );
	
	/**
	 * Mailchimp Thank you message setting
	 *
	 * - Setting: Mailchimp Thank you message
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp Thank you message.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_thank_you_message', array(
		'default'           => __( 'Thank you for subscribing. Please check your inbox for your new recipe book.', 'house' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Thank you message control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Thank you message
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp Thank you message setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_thank_you_message', array(
		'label'       => __( 'Mailchimp "Thank you" message', 'house' ),
		'description' => __( 'Wtite your "Thank you" message as success feedback for subscriber.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_thank_you_message',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp Empty field message setting
	 *
	 * - Setting: Mailchimp Empty field message
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp Empty field message.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_empty_field_message', array(
		'default'           => __( 'Address field is empty. Please type your email address and try submitting again.', 'house' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Empty field message control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Empty field message
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp Empty field message setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_empty_field_message', array(
		'label'       => __( 'Mailchimp "Empty field" message', 'house' ),
		'description' => __( 'Wtite your "Empty field" message as error feedback for subscriber.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_empty_field_message',
		'type'        => 'textarea',
	) );
}

add_action( 'customize_register', 'house_register_customizer_section_mailchimp', 11 );
