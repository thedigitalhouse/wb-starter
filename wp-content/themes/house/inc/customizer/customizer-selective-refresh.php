<?php
/**
 * Customizer: Selective refresh
 *
 * @package WordPress
 */
/**
 * Include selective refresh where applicable
 *
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_customizer_selective_refresh( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}
	/**
	 * Set Selective Refresh for blog name and description
	 *
	 * All settings with postMessage transport need custom javascript
	 * definitions as well. @see js/customizer.js
	 */
	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		/**
		 * Site name
		 */
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'            => '.site-title a',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_blogname',
		));
		/**
		 * Site description
		 */
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'            => '.site-description',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_blogdescription',
		));
		/**
		 * Footer copyrights
		 */
		$wp_customize->selective_refresh->add_partial( 'footer_copyright_text', array(
			'selector'            => '#colophon',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_footer_copyright_text',
		));
		/**
		 * Subscribe section title
		 */
		$wp_customize->selective_refresh->add_partial( 'subscribe_title', array(
			'selector'            => '.get-news-item > h3',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_subscribe_title',
		));
		/**
		 * Text before phone number
		 */
		$wp_customize->selective_refresh->add_partial( 'phone_number_prefix', array(
			'selector'            => '.contact-phone-prefix',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_phone_number_prefix',
		));
		/**
		 * Phone number
		 */
		$wp_customize->selective_refresh->add_partial( 'phone_number', array(
			'selector'            => '.contact-phone',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_phone_number',
		));
		/**
		 * Text before email address
		 */
		$wp_customize->selective_refresh->add_partial( 'email_prefix', array(
			'selector'            => '.contact-email-prefix',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_email_prefix',
		));
		/**
		 * Email address
		 */
		$wp_customize->selective_refresh->add_partial( 'contact_email_address', array(
			'selector'            => '.contact-email',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_email_address',
		));
		/**
		 * Social
		 */
		$wp_customize->selective_refresh->add_partial( 'social_profiles_', array(
			'selector'            => '.social-links',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_social',
		));
	}
}

add_action( 'customize_register', 'house_customizer_selective_refresh', 11 );
/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Render the header custom text for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_header_custom_text() {
	echo get_theme_mod( 'header_custom_text' );
}
/**
 * Render the footer copyrights for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_footer_copyright_text() {
	echo get_theme_mod( 'footer_copyright_text' );
}
/**
 * Render subscribe section title for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_subscribe_title() {
	echo get_theme_mod( 'subscribe_title' );
}
/**
 * Render the text before phone number for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_phone_number_prefix() {
	echo get_theme_mod( 'phone_number_prefix' );
}
/**
 * Render phone number for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_phone_number() {
	echo get_theme_mod( 'phone_number' );
}
/**
 * Render the text before email address for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_email_prefix() {
	echo get_theme_mod( 'email_prefix' );
}
/**
 * Render  email address for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_email_address() {
	$link = '<a href="mailto:' . get_theme_mod( 'contact_email_address' ) . '">' . get_theme_mod( 'contact_email_address' ) . '</a>';
	echo $link;
}
/**
 * Render social buttons 
 *
 * @return void
 */
function house_customize_partial_social() {
	echo get_theme_mod( 'social_profiles_' );
}