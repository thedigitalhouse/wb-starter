<?php
/**
 * Customizer: Settings and controls for 'Contact options' section
 *
 * @package WordPress
 */
/**
 * Register settings and controls for 'Contact options' section
 *
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_register_customizer_section_contact( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Phone number prefix setting
	 *
	 * - Setting: Phone number prefix
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Phone number prefix.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'phone_number_prefix', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Phone number prefix control
	 *
	 * - Control: Basic: Text
	 * - Setting: Phone number prefix
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Phone number prefix setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'phone_number_prefix', array(
		'label'       => __( 'Phone number prefix', 'house' ),
		'description' => __( 'Text before phone number.', 'house' ),
		'section'     => 'contact_options_section',
		'settings'    => 'phone_number_prefix',
		'type'        => 'text',
	) );

	/**
	 * Phone number setting
	 *
	 * - Setting: Phone number
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Phone number.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'phone_number', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Phone number control
	 *
	 * - Control: Basic: Text
	 * - Setting: Phone number
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Phone number setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'phone_number', array(
		'label'       => __( 'Phone number', 'house' ),
		'description' => __( 'Type phone number you wish to use in contact purpose.', 'house' ),
		'section'     => 'contact_options_section',
		'settings'    => 'phone_number',
		'type'        => 'text',
	) );

	/**
	 * Email address prefix setting
	 *
	 * - Setting: Email address prefix
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Email address prefix.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'email_prefix', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Email address prefix control
	 *
	 * - Control: Basic: Text
	 * - Setting: Email address prefix
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Email address prefix setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'email_prefix', array(
		'label'       => __( 'Email address prefix', 'house' ),
		'description' => __( 'Text before email address.', 'house' ),
		'section'     => 'contact_options_section',
		'settings'    => 'email_prefix',
		'type'        => 'text',
	) );

	/**
	 * Email address setting
	 *
	 * - Setting: Email address
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Email address.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'contact_email_address', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_email'
	) );

	/**
	 * Email address control
	 *
	 * - Control: Basic: Text
	 * - Setting: Email address
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Email address setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'contact_email_address', array(
		'label'       => __( 'Email address', 'house' ),
		'description' => __( 'Type email address you wish to use in contact purpose.', 'house' ),
		'section'     => 'contact_options_section',
		'settings'    => 'contact_email_address',
		'type'        => 'email',
	) );

}

add_action( 'customize_register', 'house_register_customizer_section_contact', 11 );
