/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	// Site description
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	/**
	 * Theme options panel
	 */
	/**
	 * Footer options section
	 */
	// Footer copyrights
	wp.customize( 'footer_copyright_text', function( value ) {
		value.bind( function( to ) {
			$( '#colophon' ).text( to );
		} );
	} );
	/**
	 * Branding options section
	 */
	// Wastebuster logo
	wp.customize( 'wastebuster_logo', function( value ) {
		value.bind( function( to ) {
			var $image = '<img src="' + to + '" alt="" />';
			$( '#wastebuster-logo-subscribe' ).html( $image );
		} );
	} );
	/**
	 * Contact options section
	 */
	// Text before phone number
	wp.customize( 'subscribe_title', function( value ) {
		value.bind( function( to ) {
			$( '.get-news-item > h3' ).text( to );
		} );
	} );
	// Text before phone number
	wp.customize( 'phone_number_prefix', function( value ) {
		value.bind( function( to ) {
			$( '.contact-phone-prefix' ).text( to );
		} );
	} );
	// Phone number
	wp.customize( 'phone_number', function( value ) {
		value.bind( function( to ) {
			$( '.contact-phone' ).text( to );
		} );
	} );
	// Text before email address
	wp.customize( 'email_prefix', function( value ) {
		value.bind( function( to ) {
			$( '.contact-email-prefix' ).text( to );
		} );
	} );
	// Email Address
	wp.customize( 'contact_email_address', function( value ) {
		value.bind( function( to ) {
			var $link = '<a href="mailto:' + to + '">' + to + '</a>';
			$( '.contact-email' ).html( $link );
		} );
	} );

} )( jQuery );
