<?php
/**
 * Customizer: Settings and controls for 'Social profiles' section
 *
 * @package WordPress
 */
/**
 * Register settings and controls for 'Social profiles' section
 *
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_register_customizer_section_social( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}
	/**
	 * Get default networks
	 * @var array
	 */
	$networks = house_set_social_networks();
	/**
	 * Build controls for each
	 */
	foreach ( $networks as $key => $name ) :
		/**
		 * Social Profile
		 *
		 * - Setting: Social Profile
		 * - Control: url
		 * - Sanitization: url
		 *
		 * Uses a text field to configure the user's copyright text displayed in the site footer.
		 *
		 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
		 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
		 */
		$wp_customize->add_setting( 'social_profiles_' . $key, array(
			'default'           => '',
			'type'              => 'theme_mod',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'house_sanitize_url'
		) );

		/**
		 * Social Profile control
		 *
		 * - Control: Text: URL
		 * - Setting: Social Profile
		 * - Sanitization: url
		 *
		 * Register the core "url" text control to be used to configure the Social Profiles setting.
		 *
		 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
		 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
		 */
		$wp_customize->add_control( 'social_profiles_' . $key, array(
			'label'       => $name,
			'description' => sprintf( __( 'Add your %s profile link.', 'house' ), $name ),
			'section'     => 'social_profiles_section',
			'settings'    => 'social_profiles_' . $key,
			'type'        => 'url',
		) );

	endforeach; // $networks as $network
}
add_action( 'customize_register', 'house_register_customizer_section_social', 11 );