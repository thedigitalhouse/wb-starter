<?php
/**
 * Customizer: Add Sections
 *
 * @package WordPress
 */
/**
 * Register customizer sections
 *
 * Add sections to our custom customizer panel.
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_register_customizer_sections( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Add MailChimp Section for Theme Options panel.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'mailchimp_options_section', array(
		'title' => __( 'MailChimp options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add Social profiles links for Theme Options panel.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'social_profiles_section', array(
		'title' => __( 'Social Profiles', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add Contact Section for Theme Options panel.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'contact_options_section', array(
		'title' => __( 'Contact options', 'house' ),
		'panel' => 'house_options_panel',
	) );
}

add_action( 'customize_register', 'house_register_customizer_sections', 11 );
