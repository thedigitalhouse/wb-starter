<?php
/**
 * Customizer functions
 *
 * @package WordPress
 */
include( get_template_directory() . '/inc/customizer/sanitization-callbacks.php' );
/**
 * Register custom panels
 */
 include( get_template_directory() . '/inc/customizer/customizer-panels.php' );
 /**
 * Add sections to our custom panels
 */
include( get_template_directory() . '/inc/customizer/customizer-sections.php' );
 	/**
	 * Add settings and controls to 'MailChimp options' section
	 */
	include( get_template_directory() . '/inc/customizer/customizer-section-mailchimp.php' );
	/**
	 * Add settings and controls to 'Social profiles' section
	 */
	include( get_template_directory() . '/inc/customizer/customizer-section-social.php' );
	/**
	 * Add settings and controls to 'Contact options' section
	 */
	include( get_template_directory() . '/inc/customizer/customizer-section-contact.php' );
/**
 * Include selective refresh
 */
include( get_template_directory() . '/inc/customizer/customizer-selective-refresh.php' );