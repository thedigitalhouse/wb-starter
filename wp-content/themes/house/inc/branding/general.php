<?php
/**
 * Branding general
 *
 * @package WordPress
 */
/**
 * Get site title link
 *
 * @param  string $before Custom content before link
 * @param  string $after  Custom content after link
 * @return string         Returns site title link
 */
function get_house_site_title_link( $before = '', $after = '' ) {
	$link = '<a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . get_bloginfo( 'name' ) . '</a>';

	return $before . $link . $after;
}
/**
 * Site title link
 *
 * @param  string $before Custom content before link
 * @param  string $after  Custom content after link
 * @return string         Echoes site title link
 */
function house_site_title_link( $before = '', $after = '' ) {
	echo get_house_site_title_link( $before, $after );
}