<?php
/**
 *	Functions for Jetpack plugin
 *
 *	Jetpack modules whitelist
 *	If Jetpack module is active
 *
 *	@package WordPress
 */
/**
 * Hooks
 */

add_filter( 'jetpack_get_available_modules', 'house_kill_all_the_jetpacks' );
/**
 * 	Jetpack modules whitelist
 *
 * 	This will allow all of the currently available Jetpack modules to work
 * 	normally. If there's a module you'd like to disable, simply comment it out
 * 	or remove it from the whitelist and it will no longer load.
 *
 *	This function is attached to jetpack_get_available_modules filter hook.
 *
 * 	@link   http://wpbacon.com/tutorials/disable-jetpack-modules/
 *
 *	@link http://jeremyherve.com/2013/11/19/customize-the-list-of-modules-available-in-jetpack/
 *	@link https://github.com/bradyvercher/rocketeer
 *
 */
function house_kill_all_the_jetpacks( $modules ) {
	// A list of Jetpack modules which are allowed to activate.
	$whitelist = array(
		// 'after-the-deadline',
		// 'carousel',
		// 'comments',
		// 'contact-form',
		// 'custom-content-types',
		// 'custom-css',
		// 'enhanced-distribution',
		// 'gplus-authorship',
		// 'gravatar-hovercards',
		// 'holiday-snow',
		// 'infinite-scroll',
		// 'json-api',
		// 'latex',
		// 'likes',
		// 'markdown',
		// 'minileven',
		// 'mobile-push',
		// 'monitor',
		// 'notes',
		// 'omnisearch',
		// 'photon',
		// 'post-by-email',
		// 'publicize',
		// 'protect',
		// 'publicize',
		// 'random-redirect',
		// 'related-posts',
		// 'sharedaddy',
		'shortcodes',
		// 'shortlinks',
		// 'site-icon',
		// 'social-links',
		// 'sso',
		'stats',
		// 'subscriptions',
		// 'tiled-gallery',
		// 'vaultpress',
		// 'verification-tools',
		// 'videopress',
		'widget-visibility',
		// 'widgets',
	);
	// Deactivate all non-whitelisted modules.
	$modules = array_intersect_key( $modules, array_flip( $whitelist ) );

	return $modules;
}
/**
 *	If Jetpack module is active
 *
 *	Checks if Jetpack module is active.
 *
 *	@uses get_option()
 *
 * 	<code>
 *	if ( house_jetpack_active_module( 'module-name' ) ) {
 *		// do stuff with this module
 *	}
 *	</code>
 *
 *	@param string 	$module 	Module to check.
 *	@return bool
 *
 *	@link http://wptheming.com/2013/04/check-if-jetpack-modules-are-enabled/
 *
 */
function house_jetpack_active_module( $module = '' ) {
	$jetpack_active_modules = get_option( 'jetpack_active_modules' );

	return class_exists( 'Jetpack', false ) && $jetpack_active_modules && in_array( $module, $jetpack_active_modules );
}