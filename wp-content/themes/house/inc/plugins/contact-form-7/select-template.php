<?php
/**
 * Select Form Template
 *
 * Custom functionality to add the option of multiple premade form templates
 * with Contact Form 7 plugin.
 *
 * @package WordPress
 * @subpackage Contact Form 7
 * @subpackage Contact Form 7 Honeypot
 */
/**
 * Hook
 */
add_filter( 'wpcf7_default_template', 'house_modify_default_template', 10, 2 );
add_filter( 'wpcf7_editor_panels', 'house_create_panel' );
/**
 * Accessibility message for Contact Form 7 Honeypot hidden field
 * Default: Please leave this field empty
 */
add_filter( 'wpcf7_honeypot_accessibility_message', function() {
	return __( 'Please leave this field empty', 'house' );
});

/**
 * Return template based on currently selected template
 *
 * This function is attched to 'wpcf7_default_template' filter hook.
 *
 * @param $template    Default template for Contact Form 7
 * @param $prop        Current property producing a template. (Same filter applies to features other than the default form.)
 *
 * @return $template   Returns new default template.
 */
function house_modify_default_template( $template, $prop ) {
	// set current template
	$current = ( isset( $_GET['base_form'] ) ) ? $_GET['base_form'] : 'basic';

	/**
	 * Set the form template
	 * @var string
	 */
	if ( $prop == 'form' ) :

		switch ( $current ) {
			case 'simple-form':
				/**
				 * Custom template is located in
				 * inc/plugins/contact-form-7/simple-form.php
				 * @var string
				 */
				$template = simple_form_template();

			break;

			case 'basic':
			default:
				/**
				 * Set name
				 * @var string
				 */
				$name = '[text* sender-name id:sender-name class:input class:input--primary placeholder "Name"]';
				/**
				 * Set email
				 * @var string
				 */
				$email = '[email* sender-email id:sender-email class:input class:input--primary placeholder "Email"]';
				/**
				 * Set textarea
				 * @var string
				 */
				$message = '[textarea sender-message class:input class:input--primary class:input--textarea placeholder "Message"]';
				/**
				 * Set submit button
				 * @var string
				 */
				$submit = '[submit class:btn class:btn--primary class:btn--medium "Send message"]';
				/**
				 * Anti-spam field
				 * Hidden field that should stay empty if user is human
				 * @uses Contact Form 7 Honeypot plugin
				 */
				$spam = '[honeypot sender-website]';

				$form = $name . $email;
				// Throw in hidden field is Honeypot plugin is active.
				// Wrapping into <div> will prevent CF7 to wrap field into <p>
				// and apply margins and other unwanted styling
				if ( house_is_plugin_active( 'contact-form-7-honeypot/honeypot.php' ) ) {
					$form .= $spam;
				}
				$form .= $message . $submit;

				$template = $form;
		}

		$template = '[response]' . "\n\n" . $template;

		$template = apply_filters( 'house_form_template', $template, $current );

	/**
	 * Set the email template
	 * @var string
	 */
	elseif ( $prop == 'mail' ) :

		switch ( $current ) {
			case 'simple-form':
				/**
				 * Custom template is located in
				 * inc/plugins/contact-form-7/simple-form.php
				 * @var array
				 */
				$template = simple_form_email_to_client();
			break;

			case 'basic':
			default:
				$template = simple_form_email_to_client();
		}

	/**
	 * Set the autoresponder email template
	 * @var string
	 */
	elseif ( $prop == 'mail_2' ) :

		switch ( $current ) {
			case 'simple-form':
				/**
				 * Custom template is located in
				 * inc/plugins/contact-form-7/simple-form.php
				 * @var array
				 */
				$template = simple_form_autoresponder();
			break;

			case 'basic':
			default:
				$template = simple_form_autoresponder();
		}

	endif; // $prop == 'form'

	return $template;
}

/**
 * Produce the array of sample forms available
 *
 * @param $post    Current Contact Form 7 Form object
 * @return array   Returns array of available templates
 */
function house_forms( $post ) {

	$forms = array(
		'basic'               => 'Default',
		'simple-form'         => 'Simple Contact Form',
	);

	/**
	 * Filters the array of sample forms. Insert an additional form.
	 *
	 * @param $forms array of forms.
	 * @param $post Current Contact Form 7 Form object
	 */
	return apply_filters( 'house_add_form_select', $forms, $post );
}

/**
 * Add Panel
 *
 * Add panel for selecting custom template. This function is
 * attached to 'wpcf7_editor_panels' filter hook.
 *
 * @param $panels array    Array of default panels (tabs)
 * @return array           Returns array of filtered panels
 */

function house_create_panel( $panels ) {
	if ( isset( $_GET['page'] ) && $_GET['page'] == 'wpcf7-new' ) {

		$panels['house-custom-templates'] = array(
			'title'    => __( 'Form Templates', 'house' ),
			'callback' => 'house_select_form'
		);
	}

	return $panels;
}
/**
 * Select Form
 *
 * Populate custom panel with list of all available templates.
 *
 * @param  obj $post   Current Contact Form 7 Form object
 * @return string      Returns templates panel content
 */
function house_select_form( $post ) {
	$links = '';
	// get all available forms
	$available_forms = house_forms( $post );
	// prepare base url
	$base_url = admin_url( 'admin.php?page=wpcf7-new' );

	foreach ( $available_forms as $key => $form ) {
		// set the url with query string
		$url = esc_url( add_query_arg( 'base_form', $key, $base_url ) );
		// set current template
		$current = ( isset( $_GET['base_form'] ) ) ? $_GET['base_form'] : 'basic';
		// format selected template without link
		if ( $current == $key ) {
			$links .= "<li class='selected'>$form</li>";
		} else {
			$links .= "<li><a href='$url'>$form</a></li>";
		}
	}

	if ( $links ) {
		$links = "
			<h2>". __( 'Select form template:', 'house' ) . "</h2>
				<div class='select-form-template'>
					<ul class='template-list'>
						$links
					</ul>
				</div>";

		echo $links;
	}
}