<?php
/**
 * ACF related helper functions
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields Pro
 */
/**
 * Hooks
 */
add_filter( 'acf/fields/wysiwyg/toolbars' , 'house_wysiwyg_toolbar'  );
/**
 * ACF field
 *
 * Perform the 'if empty' field check and echo it value with
 * optional content before and after it.
 *
 * @param  string $field  Field id
 * @param  boolean $echo  Whether to echo or return the value, default is true
 * @param  string $before Custom markup before field value
 * @param  string $after  Custom markup after field value
 * @return string         Returns or echoes field value
 */
function acf_field( $field = '', $echo = true, $before = '', $after = '' ) {
	/**
	 * Declare empty value to avoid notices
	 * @var string
	 */
	$value = '';
	/**
	 * Get the field value
	 */
	if ( get_field( $field ) ) :
		$value = get_field( $field );
	endif; // get_field( $field )
	/**
	 * Echo or return the value
	 */
	if ( ! empty( $value ) ) {
		if ( $echo ) {
			echo $before . $value . $after;
		} else {
			return $value;
		}
	}
}
/**
 * ACF sub field
 *
 * Perform the 'if empty' sub field check and echo it value with
 * optional content before and after it.
 *
 * @param  string $field  Field id
 * @param  boolean $echo  Whether to echo or return the value, default is true
 * @param  string $before Custom markup before sub field value
 * @param  string $after  Custom markup after sub field value
 * @return string         Returns sub field value with or without custom markup
 */
function acf_sub_field( $field = '', $echo = true, $before = '', $after = '' ) {
	/**
	 * Declare empty value to avoid notices
	 * @var string
	 */
	$value = '';
	/**
	 * Get the field value
	 */
	if ( get_sub_field( $field ) ) :
		$value = get_sub_field( $field );
	endif; // get_sub_field( $field )
	/**
	 * Echo or return the value
	 */
	if ( ! empty( $value ) ) {
		if ( $echo ) {
			echo $before . $value . $after;
		} else {
			return $value;
		}
	}
}
/**
 * ACF WYSIWYG toolbars
 *
 * Filter through ACF WYSIWYG toolbars and modify available buttons
 * or remove/add complete toolbars. By default ACF has two toolbars:
 *
 * - Full - as default WordPress visual editor (two rows, row = array)
 * - Basic - pretty reduced with one row
 *
 * We want even more reduced and to make it default. We are going
 * to unset default ones and leave only ours so it's always default.
 * This function is attached to 'acf/fields/wysiwyg/toolbars'
 * filter hook.
 *
 * @link https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/
 * @link https://www.advancedcustomfields.com/resources/adding-custom-javascript-fields/
 *
 * @param  array $toolbars   Array of existing toolbars
 * @return array             Returns filtered toolbars
 */
function house_wysiwyg_toolbar( $toolbars ) {

	// add 'Simple' toolbar
	$toolbars['Simple'] = array();
	/**
	 * If any custom buttons exist,
	 * just add them in array
	 * e.g. '|', 'custom_button'..
	 */
	$toolbars['Simple'][1] = array( 'bold', 'italic', 'link', 'unlink', 'undo', 'redo' );

	// remove 'Full' and 'Basic' toolbars completely
	unset( $toolbars['Basic'] );
	unset( $toolbars['Full'] );

	return $toolbars;
}