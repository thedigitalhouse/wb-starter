<?php
/**
 * Comments modifications
 *
 * Custom comments callbacks.
 *
 * @package WordPress
 */

/**
 * Hooks
 */
add_filter( 'comment_form_fields', 'reorder_comment_form_fields' );

/**
 * Template for comments and pingbacks list
 *
 * Used as a callback by wp_list_comments() for displaying the comments list. @see comments.php
 *
 * @param obj    $comment      Comment object
 * @param array  $args         An array of arguments for displaying comments.
 * @param int    $depth        The depth of the new comment. Must be greater than 0 and less than the value
 *                             of the 'thread_comments_depth' option set in Settings > Discussion. Default 0.
 * @return string   Returns comment markup
 */
function house_comment( $comment, $args, $depth ) {
	global $comment;

	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'house' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'house' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment">

			<div class="comment-entry">
				<div class="avatar">
					<?php
						/**
						 * Comment author avatar
						 * If size is custom, get the value
						 * @link https://codex.wordpress.org/Function_Reference/get_avatar
						 */
						if ( $args['avatar_size'] != 0 ) {
							echo get_avatar( $comment, $args['avatar_size'] );
						}
					?>
				</div>

				<div class="comment-author">
					<?php
						/**
						 * Comment author name
						 * Avoid links for spammy comments
						 * @link https://codex.wordpress.org/Function_Reference/get_comment_author
						 */
						echo esc_html( get_comment_author( $comment->comment_ID ) ); ?>
				</div>

				<div class="comment-date">
					<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>" title="Permalink to this comment">
					<?php
						/**
						 * Comment date and time
						 * translators: 1: date, 2: time
						 */
						printf( __( '%1$s at %2$s', 'house' ), get_comment_date( 'd M Y' ),  get_comment_time() ); ?>
					</a>
				</div>

				<div class="comment-reply">
					<?php
						/**
						 * Reply link
						 * Merge new args with default ones
						 * @link https://codex.wordpress.org/Function_Reference/comment_reply_link
						 */
						$reply_args = array(
							'reply_text' => __( 'Reply', 'house' ),
							'login_text' => __( 'Log in to reply.', 'house' ),
							'before'     => '',
							'after'      => '',
							'depth'      => $depth,
							'max_depth'  => $args['max_depth']
						);
						comment_reply_link( array_merge( $args, $reply_args ) ); ?>
				</div><!-- comment-reply -->

				<div class="comment-content">
					<?php if ( '0' == $comment->comment_approved ) : ?>
						<span class="comment-awaiting-moderation">
							<?php _e( 'Your comment is awaiting moderation.', 'house' ); ?>
						</span>
					<?php endif; ?>

					<?php comment_text(); ?>
				</div><!-- comment-content -->

			</div><!-- comment-entry -->

		</div><!-- #comment-<?php comment_ID(); ?> -->
	<?php
		break;
	endswitch; // end comment_type check
}

/**
 * Reorder comment form fields
 *
 * WordPress has changed the order of comment form fields in v4.4, so that
 * comment field is above the text fields for name and email.
 * @link https://make.wordpress.org/core/2015/09/25/changes-to-fields-output-by-comment_form-in-wordpress-4-4/
 *
 * Here we are putting that order back. This function is attached to 'comment_form_fields' filter hook.
 * @link http://www.wpbeginner.com/wp-tutorials/how-to-move-comment-text-field-to-bottom-in-wordpress-4-4/
 *
 * @param  array $fields  Array of form fields
 * @return array          Returns filtered array of fields
 */
function reorder_comment_form_fields( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

/**
 * Comment form args
 *
 * Custom markup and args for single comment form for single post - insight
 *
 * @return  string Returns comment form
 */
function house_comment_form() {
	global $user_identity;
	wp_get_current_user();

	$commenter     = wp_get_current_commenter();
	$req           = get_option( 'require_name_email' );
	$aria_req      = ( $req ? " aria-required='true'" : '' );
	$required_text = '';

	$args = array(
		/**
		 * unique form id
		 */
		'id_form' => 'commentform',
		/**
		 * Submit button attributes and label
		 */
		'id_submit'    => 'submit',
		'class_submit' => 'btn btn--primary',
		'label_submit' => __( 'Submit', 'house' ),
		/**
		 * Title for the form
		 */
		'title_reply'        => __( 'Leave a comment', 'house' ),
		'title_reply_to'     => __( 'Leave a comment to %s', 'house' ),
		'title_reply_before' => '<h3>',
		'title_reply_after'  => '</h3>',
		/**
		 * Cancel the reply
		 */
		'cancel_reply_link' => '<span class="assistive-text">' . __( 'Cancel', 'house' ) . '</span>',
		/**
		 * System messages
		 */
		'must_log_in' => '<div class="must-log-in">' .
							sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'house' ),
								wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
							) . '</div>',
		'logged_in_as' => '<div class="logged-in-as">' .
								sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'house' ),
									admin_url( 'profile.php' ),
									$user_identity,
									wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
							) . '</div>',
		'comment_notes_before' => '<div class="comment-notes">' .
									__( 'Your email address will not be published.', 'house' ) . ( $req ? $required_text : '' ) .
								'</div>',
		'comment_notes_after' => '<div class="form-allowed-tags assistive-text">' .
									sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'house' ),
										' <code>' . allowed_tags() . '</code>'
									) . '</div>',
		/**
		 * Comment field - textarea
		 */
		'comment_field' => '<textarea id="comment" name="comment" class="input input--primary input--textarea" placeholder="Message" ' . $aria_req . '></textarea>',
		/**
		 * Default fields
		 * We are removing Website and leaving just required ones - name and email.
		 */
		'fields' => apply_filters( 'comment_form_default_fields',
			array(
				/**
				 * Comment author' name
				 */
				'author' => '<input id="author" class="input input--primary" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"  placeholder="Name" ' . $aria_req . ' />',
				/**
				 * Comment author' email
				 */
				'email' => '<input id="email" class="input input--primary" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" placeholder="Email" ' . $aria_req . ' />'
			)
		),
	);
	/**
	 * Finally, render the form
	 *
	 * @link http://codex.wordpress.org/Function_Reference/comment_form
	 */
	comment_form( $args );
}