<?php
/**
 * Custom modifications of the the_content()
 *
 * @package WordPress
 */

/**
 * Content trim characters
 *
 * Trim content down to exact number of characters.
 *
 * @uses trim_string() @see /inc/general.php
 *
 * @param  int $length 		Number of characters
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed content string
 */
function content_trim_characters( $length, $after = '...' ) {
	$str = apply_filters( 'the_content', get_the_content() );

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = trim_string( $str, $length, $after );
	}

	return $str;
}
/**
 * Content trim words
 *
 * Trim content down to exact number of words.
 *
 * @uses trim_string() @see /inc/general.php
 *
 * @param  int $length 		Number of words
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed content string
 */
function content_trim_words( $length, $after = '...' ) {
	$str = apply_filters( 'the_content', get_the_content() );

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = wp_trim_words( $str, $length, $after );
	}

	return $str;
}