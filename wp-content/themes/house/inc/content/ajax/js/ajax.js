/**
 * AJAX for buzz section
 */
'use strict';

var $ = window.jQuery;

// General reading options - posts per page
var ppp = HOUSE_AJAX.ppp;
// Number of pages for query - 'max_num_pages'
// @see inc/content/ajax-load-more.php
var totalPages = HOUSE_AJAX.ajaxpages;
var totalP = +totalPages - 1;
// prepare selectors
var $container = $( HOUSE_AJAX.ajaxcontainer );
var button = $( HOUSE_AJAX.ajaxbutton );
// Prepare counters for pages and clicks
var pageNumber = 1;
var buttonClick = 0;
var icons = HOUSE_AJAX.icons;
/**
 * We need popups re-enabled after ajax call
 */
function openPopup() {
    $('.js-open-popup').magnificPopup({
        removalDelay: 500, //delay removal by X to allow out-animation
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        callbacks: {
          beforeOpen: function() {
             this.st.mainClass = this.st.el.attr('data-effect');
          }
        },
        midClick: true, // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        closeMarkup: "<button title='%title%' type='button' class='mfp-close'><svg role='img' class='icon icon-close'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='" + icons + "icons.svg#icon-close'></use></svg></button>",
        tClose: 'Close'
    });
}
/**
 * We need share tooltip re-enabled after ajax call
 */
function shareTooltip() {
  $('.js-tooltip-wrap > a').on('click', function(e){
    e.preventDefault();
    $(this).parent('.js-tooltip-wrap').toggleClass('is-active');
  });
}

//WINDOW ONLOAD
$(window).load(function() {

	var $ajax_container = $( $container );

	// masonry has problem calculating image size loaded with ajax which causes
	// items to overlap
	// defining 'itemSelector' again in ajax call seems to fix the issue
	// @link https://github.com/desandro/masonry/issues/369#issuecomment-41149679
	$ajax_container.imagesLoaded( function() {
		$ajax_container.isotope();
	});
});

//ON DOCUMENT READY
$(document).ready(function() {
	// blog posts popup
	if ( $('.js-open-popup').length ) {
		openPopup();
	}

// AJAX load posts
function load_posts() {
	pageNumber++;
	var str = '&pageNumber=' + pageNumber + '&ppp' + ppp + '&action=' + HOUSE_AJAX.ajaxaction;

	$.ajax({
		type: "POST",
		dataType: "html",
		url: HOUSE_AJAX.ajaxurl,
		data: str,
		success: function( data ) {
			var $ajax_container = $( $container );
			var loadedData = $( data );

			// load images before isotope is called
			// so that items don't overlap
			$ajax_container.append( loadedData ).imagesLoaded( function() {
				$ajax_container.isotope( 'appended', loadedData, function () {
					$ajax_container.isotope( 'layout' );
				});
				// re-enable popups
				openPopup();
				// re-enable share tooltips
				shareTooltip();
			});

		}, // success
		error: function( jqXHR, textStatus, errorThrown ) {
			$loader.html( jqXHR + " :: " + textStatus + " :: " + errorThrown );
		} // error
	}); // $.ajax

	return false;
} // load_posts()


	// Finally click the button
	$( button ).on( 'click', function() {

		// Count clicks on button
		buttonClick++;

		// Hide/disable the button if no more 'pages'
		// (total number of pages - 1 is equal to number of clicks)
		if ( buttonClick === totalP ) {
			$( button ).attr( 'disabled', true );
		}

		// Load more posts
		load_posts();
	}); // $( '#more_posts' ).on

});