<?php
/**
 * Scripts
 *
 * Ajax load more posts functionality. Uses House Buzz plugin.
 *
 * @package WordPress
 * @subpackage House Buzz
 */
/**
 * Hooks
 *
 * We are setting priority to higher number (20) so that action
 * is fired later and our ajax.js called after all other enqueued scripts.
 *
 * @link https://codex.wordpress.org/Function_Reference/add_action
 *
 * Translators: $hook, $callback, $priority (default 10), $accepted_args
 */
add_action( 'wp_enqueue_scripts', 'house_load_ajax_scripts', 20, 1 );
/**
 * Load ajax scripts
 *
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts
 *
 */
function house_load_ajax_scripts() {
	global $wp_styles, $globalSite;

	// register script
	wp_register_script( 'imagesloaded', $globalSite['theme_url'] . '/bower_components/imagesloaded/imagesloaded.pkgd.min.js', array(), '4.1.1', true );
	wp_register_script( 'ajax', $globalSite['theme_url'] . '/inc/content/ajax/js/ajax.js', array( 'imagesloaded' ), '1.0.0', true );
	// enqueue script
	if ( is_front_page() ){
		wp_enqueue_script( 'imagesloaded' );
		wp_enqueue_script( 'ajax' );
	}

	/**
	 * Register a global variable to be used in js files.
	 * Use the last enqueued - 'ajax' - so that all files are covered.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/wp_localize_script
	 */
	wp_localize_script( 'ajax', 'HOUSE_AJAX', array(
		/**
		 * WordPrss AJAx url
		 */
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		/**
		 * In case no posts are found
		 */
		'noposts' => __( 'No posts found', 'house' ),
		/**
		 * Posts per page set in general 'Reading Options'.
		 */
		'ppp' => $globalSite['posts_per_page'],
		/**
		 * PHP function/ajax action name stored in globals so that we don't have to worry about renaming.
		 * @see inc/content/ajax-load-more.php
		 */
		'ajaxaction' => $globalSite['ajax_action'],
		/**
		 * Get number of pages for paged archive in order to disable ajax button on time
		 * @see index.php
		 */
		'ajaxpages' => $globalSite['ajaxpages'],
		/**
		 * Set container selector
		 */
		'ajaxcontainer' => '#ajax-posts',
		/**
		 * Set button selector
		 */
		'ajaxbutton' => '#more_posts',
		/**
		 * Theme icons dir
		 */
		'icons' => get_template_directory_uri() . '/icons/',
	));
}
