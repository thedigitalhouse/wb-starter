'use strict';

var $ = window.jQuery;

    /**
     * Convert string to slug
     *
     * Remove hyphens (but not spaces) on the first replace,
     * and in the second replace condense consecutive spaces into a single hyphen.
     *
     * @{@link  http://stackoverflow.com/a/1054862}
     * @param  {[String]} text String to be slugified
     * @return {[String]}      Returns slug
     */
    function convertToSlug(text) {
        return text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
    }

(function() {
    /**
     * Useful links
     * https://code.tutsplus.com/tutorials/guide-to-creating-your-own-wordpress-editor-buttons--wp-30182
     * http://www.wpexplorer.com/wordpress-tinymce-tweaks/
     * http://stackoverflow.com/a/30098931
     * http://wordpress.stackexchange.com/questions/172853/how-disable-checkbox-when-listbox-value-changes-in-tinymce
     * https://makina-corpus.com/blog/metier/2016/how-to-create-a-custom-dialog-in-tinymce-4
     * https://jsfiddle.net/aeutaoLf/2/
     * https://jsfiddle.net/qs5hzsh2/
     * https://www.gavick.com/blog/wordpress-tinymce-custom-buttons
     * http://wordpress.stackexchange.com/questions/235020/how-to-add-insert-edit-link-button-in-custom-popup-tinymce-window
     * http://www.devsumo.com/technotes/2014/07/tinymce-4-multi-line-labels-in-popup-dialogs/
     */
    tinymce.create( 'tinymce.plugins.House', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
            /**
             * Custom links
             * Set of custom links with extra functionality
             */
            ed.addButton( 'custom_button', {
                text: 'Custom Links',
                title: 'Custom links with extra functionality',
                icon: false,
                type: 'menubutton',
                menu: [
                    // {
                    //     text: 'Tooltip link',
                    //     onclick: function() {
                    //         ed.windowManager.open({
                    //             title: 'Insert link with tooltip',
                    //             body: [
                    //                 {
                    //                     type :        'textbox',
                    //                     name :        'tooltip',
                    //                     label :       'Tooltip content',
                    //                     placeholder : 'Type the tooltip content here.',
                    //                     multiline :   true,
                    //                     minWidth :    500,
                    //                     minHeight :   50,
                    //                 },
                    //                 {
                    //                     type :        'textbox',
                    //                     name :        'link',
                    //                     label :       'Link URL',
                    //                     placeholder : 'Paste your url here.',
                    //                 }
                    //             ],
                    //             onsubmit: function( e ) {
                    //                 var selected_text = ed.selection.getContent();

                    //                 if ( selected_text ) {
                    //                     ed.insertContent( '<a href="' + e.data.link + '"><span class="link-desc">' + e.data.tooltip + '</span> ' + selected_text + '</a>');
                    //                 } else {
                    //                     alert("Please first select piece of text which will be used as link label.");
                    //                 }
                    //             } // onsubmit
                    //         }); //ed.windowManager.open()
                    //     }, // onclick
                    // }, // Tooltip link
                    // {
                    //     text: 'Table Of Contents Link',
                    //     onclick: function() {
                    //         // http://www.wpexplorer.com/wordpress-tinymce-tweaks/
                    //         ed.windowManager.open({
                    //             title: 'Table Of Contents Link',
                    //             body: [
                    //                 {
                    //                     type: 'label',
                    //                     name: 'toc_help',
                    //                     text: "Make sure you have at least one heading on this page. If you do have and this",
                    //                 },
                    //                 {
                    //                     type: 'label',
                    //                     name: 'toc_help_1',
                    //                     text: "list is still empty, Update or Publish page first.",
                    //                 },
                    //                 {
                    //                     type: 'label',
                    //                     name: 'toc_help_2',
                    //                     text: "Also, make sure you have selected text in editor. It will be used as clickable text.",
                    //                 },
                    //                 {
                    //                     type: 'listbox',
                    //                     name: 'selectbox',
                    //                     label: 'Headings',
                    //                     values: allHeadings,
                    //                     minWidth: 300,
                    //                 },
                    //             ], //body
                    //             onsubmit: function( e ) {
                    //                 // get selected content, it'll be link label
                    //                 var selected_text = ed.selection.getContent();

                    //                 if ( selected_text ) {
                    //                     ed.insertContent( '<a href="#' + e.data.selectbox + '">' + selected_text + '</a>');
                    //                 } else {
                    //                     alert("Please first select piece of text which will be used as link label.");
                    //                 }
                    //             } //onsubmit
                    //         }); //ed.windowManager.open()
                    //     }, //onclick
                    // }, //Table Of Ccontents Link
                ] // menu
            }); //ed.addButton('custom-links')

            /**
             * Message box button
             * Set of helpers, specific for Message Box component
             */
            ed.addButton('message_box', {
                text: 'Mesage Box',
                title: 'Helpers for Message Box component',
                icon: false,
                type: 'menubutton',
                menu: [
                    // {
                    //     text: 'Placeholder text',
                    //     onclick: function() {
                    //         // http://www.wpexplorer.com/wordpress-tinymce-tweaks/
                    //         ed.windowManager.open({
                    //             title: 'Placeholder text for Message box component',
                    //             body: [
                    //                 {
                    //                     type: 'label',
                    //                     name: 'placeholder_help',
                    //                     text: "Text inside {curly} or [square] brackets. Do include brackets as well.",
                    //                 },
                    //                 {
                    //                     type: 'label',
                    //                     name: 'placeholder_help_1',
                    //                     text: "Text will show in editor as grey but don't worry, it'll be correct color on frontend.",
                    //                 },
                    //                 {
                    //                     type: 'textbox',
                    //                     name: 'placeholder',
                    //                     label: 'Placeholder text',
                    //                     value: ed.selection.getContent(),
                    //                     minWidth: 400,
                    //                 },
                    //                 {
                    //                     type: 'checkbox',
                    //                     name: 'break_before',
                    //                     label: 'Break line',
                    //                     text: ' before placeholder',
                    //                     checked: false,
                    //                 },
                    //                 {
                    //                     type: 'checkbox',
                    //                     name: 'break_after',
                    //                     label: 'Break line',
                    //                     text: ' after placeholder',
                    //                     checked: false,
                    //                 },
                    //             ], //body
                    //             onsubmit: function( e ) {
                    //                 var lineBreak = '<br>';
                    //                 var return_text = '';
                    //                 // if we want line break before
                    //                 if (e.data.break_before) {
                    //                     return_text += lineBreak;
                    //                 }
                    //                 // build placeholder text markup
                    //                 return_text += '<span class="colored-span">' + e.data.placeholder + '</span>';
                    //                 // if we want line break after
                    //                 if (e.data.break_after) {
                    //                     return_text += lineBreak;
                    //                 }
                    //                 // finally insert content
                    //                 ed.insertContent( return_text );
                    //             } //onsubmit
                    //         }); //ed.windowManager.open()
                    //     }, // onclick
                    // }, // Placeholder text
                    // {
                    //     text: 'Heading',
                    //     onclick: function() {
                    //         var selected_text = ed.selection.getContent();
                    //         var return_text = '';
                    //         return_text = '<h5>' + selected_text + '</h5>';
                    //         ed.insertContent( return_text );
                    //     }, // onclick
                    // }, // heading
                ] // menu
            }); //ed.addButton('message_box')
        }, // init

        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl : function(n, cm) {
            return null;
        },

        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                longname: 'House Custom Buttons',
                author: 'Milana',
                infourl: 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
                version: "0.1"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add( 'house', tinymce.plugins.House );

})();