<?php
/**
 * Content buzz
 *
 * Template part for displaying buzz content - blog and tweet posts.
 * Used for home page.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'grid-item' ); ?>>

	<?php
		/**
		 * Post type post
		 */
		if ( get_post_type() === 'post' ) :
			/**
			 * Tweets
			 */
			if ( get_post_format() === 'status' ) :
				/**
				 * Get tweet author username
				 *
				 * Translators: custom field id, echo or return, before, after
				 * @var string
				 */
				$username = custom_meta( 'tweet_author_username', false );
				/**
				 * Get tweet permalink
				 * Translators: custom field id, echo or return, before, after
				 * @var string
				 */
				$tweet = custom_meta( 'tweet_permalink', false ); ?>

				<div class="buzz buzz-twitter">
					<div class="buzz-info">

					<?php
						/**
						 * Get the twitter name
						 * Translators: custom field id, echo or return, before, after
						 */
						custom_meta( 'tweet_author_name', true, '<h4 class="entry-title"><a href="http://twitter.com/' . $username . '" target="_blank">', '</a></h4>' );
						/**
						 * And twitter username
						 * Translators: custom field id, echo or return, before, after
						 */
						custom_meta( 'tweet_author_username', true, '<a href="http://twitter.com/' . $username . '" target="_blank">&#64;', '</a>' ); ?>

						<div class="entry-content">
							<?php the_content(); ?>
							<div class="more-info">
								<a href="<?php echo $tweet; ?>" class="posted-on" target="_blank">
									<time class="entry-date" datetime="<?php echo esc_attr( get_the_date('c'), get_the_ID() ); ?>">
										<?php echo get_the_date( 'F j, Y' , get_the_ID() ); ?>
									</time>
								</a>
								<a href="<?php echo $tweet; ?>" target="_blank"><?php echo house_svg_icon( 'twitter' ); ?></a>
							</div><!-- /.more-info -->
						</div>
					</div><!-- buzz-info -->
				</div><!-- buzz buzz-twitter -->

		<?php
			/**
			 * Standard posts
			 */
			else : ?>
				<div class="buzz buzz-blog">

					<?php
						/**
						 * Get the featured image
						 * if one is set
						 */
						if ( has_post_thumbnail() ) {
							/**
							 * Translators: image size, string or array of attributes
							 */
							the_post_thumbnail( 'medium', array( 'alt' => the_title_attribute( 'echo=0' ) ) );
						}
					?>

					<div class="buzz-info">

						<span class="posted-on">
							<time class="entry-date" datetime="<?php echo esc_attr( get_the_date('c'), get_the_ID() ); ?>">
								<?php echo get_the_date( 'F j, Y' , get_the_ID() ); ?>
							</time>
						</span>

						<?php
							/**
							 * Get post title
							 */
							the_title( '<h4 class="entry-title">', '</h4>' ); ?>

						<div class="entry-content">
							<?php the_excerpt(); ?>
							<div class="more-info">
								<a href="#popup-<?php the_ID(); ?>" class="read-more js-open-popup" data-effect="mfp-3d-unfold">Read more</a>
								<?php get_template_part( 'partials/site/global', 'share' ); ?>
							</div><!-- /.more-info -->
						</div><!-- entry-content -->

					</div><!-- buzz-info -->
				</div><!-- buzz buzz-blog -->

				<div id="popup-<?php the_ID(); ?>" class="buzz-popup mfp-with-anim mfp-hide">
					<article>
						<?php
							/**
							 * Get the featured image
							 * if one is set
							 */
							if ( has_post_thumbnail() ) : ?>

								<div class="wp-post-image" style="background-image: url('<?php the_post_thumbnail_url( 'large' ); ?>');"></div>
						<?php endif; // has_post_thumbnail() ?>

						<header class="entry-header">
							<span class="posted-on">
								<time class="entry-date" datetime="<?php echo esc_attr( get_the_date('c'), get_the_ID() ); ?>">
									<?php echo get_the_date( 'F j, Y' , get_the_ID() ); ?>
								</time>
							</span>
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</header><!-- /.entry-header -->

						<div class="entry-content">
							<?php the_content(); ?>
						</div><!-- /.entry-content -->

						<footer class="entry-footer">
							<div class="more-info">
								<?php get_template_part( 'partials/site/global', 'share' ); ?>
							</div><!-- /.more-info -->
						</footer><!-- /.entry-footer -->

					</article>
				</div><!-- /.buzz-popup -->

			<?php endif; // get_post_format() === 'status'

		/**
		 * Get post type 'report'
		 */
		elseif ( get_post_type() === 'report' ) : ?>

			<div class="buzz buzz-instagram">
				<div class="wrap">
					<?php
						/**
						 * Get the featured image
						 * if one is set
						 */
						if ( has_post_thumbnail() ) {
							/**
							 * Translators: image size, string or array of attributes
							 */
							the_post_thumbnail( 'reports', array( 'alt' => the_title_attribute( 'echo=0' ) ) );
						}
					?>

					<div class="instagram-user">
						<?php
							/**
							 * Get school name and number
							 */
							$number = custom_meta( 'report_number', false );
							custom_meta( 'report_school', true, '<h4>', '</h4>' );
							echo '<div><strong>' . number_format( $number ) . '</strong> ' . __( 'Bananas saved', 'house' ) . '</div>';
						?>
					</div><!-- instagram-user -->

				</div><!-- wrap -->
			</div><!-- /.buzz -->

	<?php endif; // get_post_type() === 'post' ?>

</article><!-- #post -->