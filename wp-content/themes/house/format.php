<?php
/**
 * Format
 *
 * The default template for displaying content for post formats.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * Get post title
		 */
		get_template_part( 'partials/content/title-singular' );

		/**
		 * Get flexible content fields if any,
		 * otherwise get regular content
		 */
		if ( get_field( 'content_fields' ) ) :

			get_template_part( 'partials/flexible/sections' );

		else :

			get_template_part( 'partials/content/post-content' );

		endif; // get_field( 'content_fields' )

		/**
		 * Get post author meta
		 */
		get_template_part( 'partials/meta/post-author' );
	?>

</article><!-- #post -->