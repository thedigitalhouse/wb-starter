<?php
/**
 * The template for displaying all pages by default.
 *
 * This template is without sidebar. For adding sidebars on each side, use page templates.
 *
 */
get_header();

	/**
	 * Get the featured image
	 * if one is set
	 */
	if ( has_post_thumbnail() ) {
		/**
		 * Translators: image size, string or array of attributes
		 */
		the_post_thumbnail( 'full', array( 'alt' => the_title_attribute( 'echo=0' ) ) );
	}

	// start loop
	while ( have_posts() ) : the_post();

		get_template_part( 'content', 'page' );

	endwhile; // end of the loop.

get_footer();