#!/bin/bash

wp core download

wp core config --dbname=DBNAME --dbuser=DBUSER --dbpass=DBPASSWORD

wp core install --url=LOCALURL --title=TITLE --admin_user=dev --admin_password=dev@local --admin_email=EMAIL@WEBSITE.COM

sudo npm install -g bower

sudo npm install -g gulp

sudo npm cache clean && npm update --save

bower install

gulp build

composer install

wp theme activate house

wp plugin delete hello